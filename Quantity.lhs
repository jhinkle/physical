This is an implementation of "physical quantities": scalars and vectors with
associated uncertainty (Gaussian standard deviation) and units.  Facilities are
available to do mathematics using these quantities and to do unit conversion.

> module Quantity where

First we'll define what a unit is.  A compound unit (what we'll later refer to
simply as Unit) is a vector of SIBaseUnit types, each raised to some Floating
power.  SIBaseUnits are the SI base units in the physical dimensions (Length,
Time, Mass, etc).  Before that we need dimensions.  After that we'll define
scalars and go from there

Dimensions
==========

> data Dimension = Length | Time | Mass | Temperature | ElectricCurrent | LuminousIntensity

Now a couple ways to print dimensions (short or long)

> printDim :: Dimension -> String
> printDim Length = "Length"
> printDim Time = "Time"
> printDim Mass = "Mass"
> printDim Temperature = "Temperature"
> printDim ElectricCurrent = "Electric Current"
> printDim LuminousIntensity = "Luminous Intensity"

> printDimAbbrev :: Dimension -> String
> printDimAbbrev Length = "L"
> printDimAbbrev Time = "t"
> printDimAbbrev Mass = "M"
> printDimAbbrev Temperature = "T"
> printDimAbbrev ElectricCurrent = "C"
> printDimAbbrev LuminousIntensity = "I"

Units
=====

OK, now for each of these units we'll have a base unit.

> data SIBaseUnit = SIBU String String [String] Dimension

And a few accessor functions to get the name, abbreviation and dimension of a
base unit:

> unitNameBase :: SIBaseUnit -> String
> unitNameBase (SIBU n _ _ _) = n

> unitAbbrevBase :: SIBaseUnit -> String
> unitAbbrevBase (SIBU _ a _ _) = a

> dimensionBase :: SIBaseUnit -> Dimension
> dimensionBase (SIBU _ _ _ d) = d

Now we need a function that gives us the base unit for a given dimension

> baseOfDim :: Dimension -> SIBaseUnit
> baseOfDim Length = SIBU "meters" "m" [] Length
> baseOfDim Time = SIBU "seconds" "s" ["sec"] Time
> baseOfDim Mass = SIBU "kilograms" "kg" [] Mass
> baseOfDim Temperature = SIBU "Kelvin" "K" [] Temperature
> baseOfDim ElectricCurrent = SIBU "Amperes" "A" [] ElectricCurrent
> baseOfDim LuminousIntensity = SIBU "candelas" "cd" [] LuminousIntensity

Compound Units
==============

OK now we need a way to combine base units to make compound or derived units.
All the units are combinations of zero or more base units raised to some,
possibly non-integer, power.

> type Unit = Un String String [String] (Maybe Float, Maybe Float, Maybe Float, Maybe Float, Maybe Float, Maybe Float)

This describes a single unit.  We need to be able to multiply and divide units.

Finally, let's make it so that we can print units

Scalar Quantities
=================

Luckily all we need to define is the scalar type, then Vector will take care of
extension to higher dimensions.

> data ScalarQuantity = SQ Float Float Unit

The type parameters are the numeric type used to represent the quantity, and the
Unit type.
