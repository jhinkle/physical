import System.IO (hFlush,stdout)

import Control.Monad.State
import Control.Monad.RWS
import qualified Data.Map as Map

import Physical.Parse
import Physical.Expression

-- these are for determining where to put user config files
import System.FilePath.Posix ((</>))
import System.Directory (getHomeDirectory)

-- the haskeline user input library
import System.Console.Haskeline

-- |haskeline settings, specific to physicali
hlSettings :: IO (Settings IO)
hlSettings = do
    hd <- getHomeDirectory
    return Settings { complete = completeFilename,
               historyFile = Just $ hd </> ".physicali_hist",
               autoAddHistory = True }

-- |main just runs our repl, initialized with an empty WS
main :: IO ()
main = void $ do
    s <- hlSettings
    runInputT s $ runStateT replLoop Map.empty

-- |this is just a single loop of the repl
replLoop :: StateT Workspace (InputT IO) ()
replLoop = do
    -- get a line using haskeline
    maybeLine <- lift $ getInputLine promptStr
 
    case maybeLine of
        Nothing -> lift $ outputStrLn "Exiting..."
        Just l -> do
            -- run that line with our current state
            ws <- get
            let
                (wsmod, retStr) = execRWS replLine l ws
            put wsmod

            -- print any output that was returned
            lift $ outputStrLn retStr

            -- go again
            replLoop
  where
    promptStr = ">> "  -- for customizing prompt

-- |take a line of input and get the output
replLine :: RWS String String Workspace ()
replLine = do
    l <- ask
    case parseStatement l of
        Left err -> tell $ "Statement parse error: " ++ err
        Right st -> tell =<< runStatement (return st)
