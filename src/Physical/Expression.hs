{-# LANGUAGE FlexibleContexts #-}
module Physical.Expression
    (
    Workspace,
    Statement (..),
    Expr (..),
    UnaryOp (..),
    BinaryOp (..),
    unOpFun,
    binOpFun,
    runExpr,
    runVarExpr,
    expandVars,
    runStatement
    ) where

import Control.Monad.State
import Control.Applicative
import qualified Data.Map as Map

import Physical.Quantity

-- |just a collection of uniquely named quantities
type Workspace = Map.Map String Quantity

-- |Possible commands.  
-- Can assign variables, run keywords, or evaluate expressions
data Statement = String := Expr  -- ^ assignment
               | Keyword String [String] -- ^ keyword like help, who, etc
               | Evaluate Expr -- ^ evaluate and print this expression
               {- Seq [Statement]  -- list of statements-}
               deriving (Show, Eq)

-- |expression type
data Expr = C Quantity -- ^ constant value
    | V String -- ^ named variable
    | Un UnaryOp Expr -- ^ unary operation
    | Bin BinaryOp Expr Expr -- ^ binary operation
        deriving (Show, Eq)

-- |unary operator type
data UnaryOp = Neg deriving (Show, Eq)
-- |binary operator type
data BinaryOp = Add | Sub | Mul deriving (Show, Eq)

-- |take operator types and get a corresponding Haskell function
unOpFun :: UnaryOp -> Quantity -> Quantity
unOpFun Neg = negate
binOpFun :: BinaryOp -> Quantity -> Quantity -> Quantity
binOpFun Add = (+)
binOpFun Sub = (-)
binOpFun Mul = (*)

-- |function to run an Expr AST.
-- Left values are error strings
runExpr :: Expr -> Either String Quantity
runExpr (C q) = Right q
runExpr (V s) = Left $ "Cannot run unsubstituted variable " ++ s
runExpr (Un o e) = unOpFun o <$> runExpr e
runExpr (Bin o a b) = binOpFun o <$> runExpr a <*> runExpr b

-- |replace variables with values in an expression
expandVars :: Workspace -> Expr -> Either String Expr
expandVars _ (C q) = Right $ C q
expandVars ws (V s) = maybe 
            (Left $ "Undefined variable " ++ s)
            (Right . C)
            $ Map.lookup s ws
expandVars ws (Un o e) = Un o <$> expandVars ws e
expandVars ws (Bin o a b) = Bin o <$> expandVars ws a <*> expandVars ws b

-- |evaluate an expression that might have variables in it
runVarExpr :: Workspace -> Expr -> Either String Quantity
runVarExpr ws e = runExpr =<< expandVars ws e

-- |take a parsed statement and run it
runStatement :: MonadState Workspace m => m Statement -> m String
runStatement stw = do
    st <- stw -- get the non-monadic statement part
    ws <- get -- get the state part
    case st of 
        (n := e) -> either 
                return
                {-(\val -> ((modify $ Map.insert n val) =<< (show val)))-}
                (\val -> do
                    -- use modify we don't have to extract state (probably)
                    modify $ Map.insert n val
                    {-put $ (Map.insert n val ws)-}
                    return $ show val)
                $ runVarExpr ws e
        (Evaluate e) -> return $ either id show $ runVarExpr ws e
        (Keyword _ _) -> return $ show ws
        --(Keyword _ _) -> return $ "No keyword support just yet"
