module Physical.Parse
    ( parseStatement
    , parseExpr
    ) where

import Control.Monad
import Control.Applicative ((<*))

import Text.Parsec
import Text.Parsec.String (Parser)
import Text.Parsec.Expr (Operator(..),Assoc(..),buildExpressionParser)
import Text.Parsec.Token
import Text.Parsec.Language (emptyDef)

import Physical.Expression

-- be fancy and use the Bifunctor instance of Either
-- cf: https://www.fpcomplete.com/user/liyang/profunctors
class Bifunctor f where
    bimap :: (a -> b) -> (c -> d) -> f a c -> f b d

instance Bifunctor Either where
    bimap g h = either (Left . g) (Right . h)

-- |parse an expression
parseExpr :: String -> Either String Expr
parseExpr s = bimap show id $ parse exprParser "" s

-- |parse a statement, which can contain expressions, keywords, etc
parseStatement :: String -> Either String Statement
parseStatement s = bimap show id $ parse stmtParser "" s

-- |language definition
physicalDef :: LanguageDef st
{-physicalDef = javaStyle  -- our syntax is just like java-}
physicalDef = emptyDef{ commentStart = "/*"
              , commentEnd = "*/"
              , commentLine = "//"
              , nestedComments = True
              , identStart = letter
              , identLetter = alphaNum <|> char '_'
              , reservedOpNames = ["=", "+", "-", "*", "::"]
              , reservedNames = []
              , caseSensitive = True
              }

TokenParser{ parens = m_parens
           {-, braces = m_braces-}
           {-, angles = m_angles-}
           {-, brackets = m_brackets-}
           {-, colon = m_colon-}
           , integer = m_integer
           , identifier = m_identifier
           , reservedOp = m_reservedOp
           {-, reserved = m_reserved-}
           {-, semiSep1 = m_semiSep1-}
           , whiteSpace = m_whiteSpace } = makeTokenParser physicalDef

exprParser :: Parser Expr
exprParser = buildExpressionParser table term <?> "expression"
    where
        table = [ [binary "*" (Bin Mul) AssocLeft]
                , [binary "+" (Bin Add) AssocLeft , binary "-" (Bin Sub) AssocLeft]
                ]
        binary name fun = Infix (do{ m_reservedOp name; return fun })
        term = m_parens exprParser
               <|> liftM C m_integer
               <|> liftM V m_identifier

stmtParser :: Parser Statement
stmtParser = m_whiteSpace >> stmtp <* eof
    where 
      stmtp :: Parser Statement
      stmtp = do { v <- m_identifier
                     ; m_reservedOp "="
                     ; e <- exprParser
                     ; return (v := e)
                     }
              <|> do { e <- exprParser
                     ; return (Evaluate e)
                     }
