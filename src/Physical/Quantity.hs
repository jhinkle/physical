module Physical.Quantity
    (
    Quantity
    ) where

-- |for now let's just handle integers
type Quantity = Integer
