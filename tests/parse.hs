{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# LANGUAGE TemplateHaskell #-}
-----------------------------------------------------------------------------
-- |
-- Module : TestParse (hunit)
-- Copyright : (C) 2013 Jacob Hinkle
-- License : BSD-style (see the file LICENSE)
-- Maintainer : Jacob Hinkle <jacob@sci.utah.edu>
-- Stability : provisional
-- Portability : portable
--
-- This module provides a simple hunit test suite for parsing physical
-- expressions and statements
-----------------------------------------------------------------------------
module Main where

import Physical.Expression as E
import Physical.Parse as P

import Test.Framework.TH
import Test.Framework
import Test.HUnit
import Test.Framework.Providers.HUnit

main :: IO ()
main = $(defaultMainGenerator)

-- Assignment
case_parse_assign_one = do
    Right ("a" E.:= E.C 1) @=? P.parseStatement "a=1"
case_parse_assign_one_paren = do
    Right ("a" E.:= E.C 1) @=? P.parseStatement "a= (1)"
case_parse_assign_var = do
    Right ("a" E.:= E.V "b") @=? P.parseStatement "a=b"
case_parse_assign_var_paren = do
    Right ("a" E.:= E.V "b") @=? P.parseStatement "a = (b)"

-- Keywords

-- Below (mixed together) are a bunch of Evaluate tests also

-- Constants
case_parse_constant_one = do
    Right (E.C 1) @=? P.parseExpr "1"
case_parse_constant_one_paren = do
    Right (E.C 1) @=? P.parseExpr "(1)"
case_parse_statement_const_one = do
    Right (Evaluate (E.C 1)) @=? P.parseStatement "1"
case_parse_statement_const_one_paren = do
    Right (Evaluate (E.C 1)) @=? P.parseStatement "(1)"

-- Variable names
case_parse_expr_varname = do
    Right (E.V "a") @=? P.parseExpr "a"
case_parse_expr_varname_paren = do
    Right (E.V "a") @=? P.parseExpr "(a)"
case_parse_statement_varname = do
    Right (Evaluate (E.V "a")) @=? P.parseStatement "a"
case_parse_statement_varname_paren = do
    Right (Evaluate (E.V "a")) @=? P.parseStatement "(a)"
case_parse_statement_varname_longer = do
    Right (Evaluate (E.V "abcde")) @=? P.parseStatement "abcde"
case_parse_statement_varname_longer_paren = do
    Right (Evaluate (E.V "abcde")) @=? P.parseStatement "(abcde)"

-- Binary ops
